# **Nixxis MediaServer Script**

> **Nixxis MediaServer Script** is a shell script that allows you to install all the needed component for Nixxis on Centos 6, Centos 7 and RHEL 7 environments. 

## Download Script

As root, download the script and patches files on the server to install

For Centos7
```bash
sudo -s
wget --output-document=nms.sh --no-clobber https://bitbucket.org/NixxisSupport/nixxis-nms-installation/raw/1a0a7ac0b9277cc0eea65b8dbc60e7706bebb8bc/nms_centOS7_Asterisk11.sh
```
or for RHEL7
```bash
wget --output-document=nms.sh --no-clobber https://bitbucket.org/NixxisSupport/nixxis-nms-installation/raw/9fdcf78a26507f454598c101e9e590120653469d/nms_Asterisk11.sh
```
or for Centos6
```bash
wget --output-document=nms.sh --no-clobber https://bitbucket.org/NixxisSupport/nixxis-nms-installation/raw/1a0a7ac0b9277cc0eea65b8dbc60e7706bebb8bc/nms_centOS6.bash
```

Grant execution permissions on the script

```bash
chmod +x nms.sh
```

Convert to Unix format

```bash
dos2unix nms.sh
```

Execute the script

```bash
sudo bash nms.sh
```

## Install Nixxis Component

> The first option for this script is for performing the installation of Media Server

### Execute

To perform the installation, run the script with argument **install**

```bash
sudo bash nms.sh -install
```

The script will execute various operation that can be separate in 5 steps

#### Get configuration

#### Check prerequisites and initial setup

#### Installing needed tools

#### Installing Asterisk

#### Deploying Nixxis configuration and Dialplans



## Check Nixxis MediaServer

> Another option for this script is for performing the checkup of Media Server.

### Execute

To perform the checkup, run the script with argument **check**

```bash
sudo bash nms.sh -check
```

The script will check if every thing is well installed and running and will print some realtime information on the status of the media server.